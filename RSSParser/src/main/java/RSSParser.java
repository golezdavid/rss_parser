import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.sql.*;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

//BRANJE RSS S ROME/ ZAPISOVANJE V PODATKOVNO BAZO

public class RSSParser {

    public static void main(String[] args) throws Exception {

        //URL RSS
        URL url = new URL("https://www.komusg.si/DesktopModules/DNNGo_xBlog/Resource_Ajax.aspx?ModuleId=454&ajaxType=2&TabId=92&PortalId=0&language=sl-SI");
        XmlReader xmlReader = null;

        try {
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/novice", "root", "root");

                Statement statement = connection.createStatement();


            xmlReader = new XmlReader(url);
            SyndFeed feeder = new SyndFeedInput().build(xmlReader);
            System.out.println("Title Value " + feeder.getAuthor());


            //BRIŠI VSEBINO TABELE
            String query = "Truncate table novice_komunala";
            //Executing the query
            statement.execute(query);
            //ITERACIJA ZA PRIDOBIVANJE PODATKOV IZ RSS
            for (Iterator iterator = feeder.getEntries().iterator(); iterator
                    .hasNext();) {
                SyndEntry syndEntry = (SyndEntry) iterator.next();
                System.out.println(syndEntry.getTitle());
                System.out.println(syndEntry.getLink());
                System.out.println(syndEntry.getPublishedDate());
                System.out.println(syndEntry.getDescription().getValue());


                //PODATKI V SPREMENLJIVKAH
                String title = syndEntry.getTitle();
                String link = syndEntry.getLink();
                Date date = syndEntry.getPublishedDate();
                String description = syndEntry.getDescription().getValue();
                java.sql.Date d=new java.sql.Date(date.getTime());


                System.out.println(description);



                //VSTAVI V TABELO PODATKE
                String sql = "INSERT INTO novice_komunala(title, link, pubdate, description) values (?, ?, ?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(sql);

                preparedStatement.setString(1, title);
                preparedStatement.setString(2, link);
                preparedStatement.setDate(3, d);
                preparedStatement.setString(4, description);

                int rows = preparedStatement.executeUpdate();

                if (rows > 0) {
                    System.out.println("Vstavljeno uspešno");
                }
            }
        } finally {
            if (xmlReader != null)
                xmlReader.close();
        }
    }
}
